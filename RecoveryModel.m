function omega=RecoveryModel(phi,t)

beta=0.9;
omega=phi(1)*(1./(1+exp(-beta*(t-phi(2))))-1./(1+exp(beta*phi(2))))...
     +phi(3)./(1+exp(-8*(t-0.5)));%*(1./(1+exp(-  4*(t-phi(3))))-1);
