function [sig_interv3ss,sig_interv4ss,sig_interv5ss,sig_interv6ss,...
          sig_interv7ss,sig_interv8ss,sig_double,sig_sham]=...
          ModelSignal(params,sigRec,rescale3ss,rescale4ss,rescale5ss,...
                      sigSham3ss,sigSham4ss,sigSham5ss,sigSham6ss,sigSham7ss,sigSham8ss)

zz=0*sigSham7ss;
w=abs([0 0 params(1) params(2) params(3) params(4) params(5) params(6)]); % weighting of each stage (1ss, 2ss, 3ss, 4ss, 5ss, 6ss, 7ss, 8ss)
% recovery signals at 4ss and 5ss need rescaling based on motile cilia
% percentage because no observations available (rescale4ss and rescale5ss)
% sham signals at 3ss, 4ss and 5ss are already rescaled (sigSham3ss,
% sigSham4ss, sigSham5ss)
sig_interv3ss=[zz zz zz          rescale4ss*sigRec(:,1) rescale5ss*sigRec(:,2) sigRec(:,3) sigRec(:,4) sigRec(:,5)]*w';
sig_interv4ss=[zz zz sigSham3ss  zz                     rescale5ss*sigRec(:,1) sigRec(:,2) sigRec(:,3) sigRec(:,4)]*w'; 
sig_interv5ss=[zz zz sigSham3ss  sigSham4ss             zz                     sigRec(:,1) sigRec(:,2) sigRec(:,3)]*w';
sig_interv6ss=[zz zz sigSham3ss  sigSham4ss             sigSham5ss             zz          sigRec(:,1) sigRec(:,2)]*w';
sig_interv7ss=[zz zz sigSham3ss  sigSham4ss             sigSham5ss             sigSham6ss  zz          sigRec(:,1)]*w';
sig_interv8ss=[zz zz sigSham3ss  sigSham4ss             sigSham5ss             sigSham6ss  sigSham7ss  zz         ]*w';
sig_double   =[zz zz sigSham3ss  sigSham4ss             zz                     sigRec(:,1) zz          sigRec(:,1)]*w';
sig_sham     =[zz zz sigSham3ss  sigSham4ss             sigSham5ss             sigSham6ss  sigSham7ss  sigSham8ss ]*w';
