function PrettyPrint(fig,wd,ht,fs,xtext,ytext,format,filename)

axx=get(fig,'Children');
set(fig,'paperunits','centimeters');
set(fig,'papersize',[wd ht]);
set(fig,'paperposition',[0 0 wd ht]);
for j=1:length(axx)
    set(axx(j),'fontsize',fs);
    box on;
    axtemp=axx(j).Type;
    axtemp=axtemp(1);
    if(axtemp=='a')
        set(axx(j),'tickdir','out');
    end
end
hx=xlabel(xtext,'interpreter','latex');
hy=ylabel(ytext,'interpreter','latex');
set(hx,'fontsize',fs);
set(hy,'fontsize',fs);
print(fig,format,'-r600',filename);
