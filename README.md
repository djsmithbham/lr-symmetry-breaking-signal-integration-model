# LR symmetry breaking signal integration model

Supplementary Matlab code to accompany the manuscript

*Fluid extraction from the left-right organizer uncovers mechanical properties needed for symmetry breaking,* Pedro Sampaio, Sara Pestana, Catarina Bota, Adán Guerrero, Ivo A. Telley, David. J. Smith, Susana S. Lopes (2023)

*Currently under review for publication.*

Scripts were written for Matlab R2021a, Mathworks, Inc, Natick MA

Code made publicly available under a CC-BY-4.0 licence 10th April 2023

## How to use

To run the code, execute the function fitInterventionModel.m in Matlab

The code takes around 15 minutes to execute on a typical notebook computer.

## Dependencies
Required Matlab toolboxes: 
* Statistics and Machine Learning
* Optimization
* Global Optimization

## Overview

The main function reads in angular velocity data from intervention experiments along with
data on motile cilia percentages over time
then fits a signal integration model and the effect of intervention to
the data.

The output consists of figure panels showing the best fit to data and
associated parameter values (stage weightings), along with relative stage
weightings multiplied by angular velocities (stage contributions)
 
The panels comprise Figure 6 of the main manuscript.

## Detailed summary
 
### Input data files
| Filename | Description |
| :---     | :---        |
| av_data.csv | angular velocity data |
| motile_percentages_saved.csv | data on changes in the percentate of cilia that are motile over time |

*Please note that data on number of embryos with defects and total number of embryos is given below within the script file fitInterventionModel.m*

### Variable to be specified by user
| Variable | Options |
| :---     | :---    |
| model_version | can be set as 'full', 'no3ss', 'no3-4ss', 'no3-5ss', 'no3-6ss' to test the alternative models with no      contribution before 4, 5, 6 and 7ss for comparison. All of these alternatives were tested to generate Figure 6 of the main manuscript |

### Main script file
fitInterventionModel.m

### Associated function files
| Filename | Description |
| :---     | :---        |
| ModelFun.m | statistical model of defect rate under a given biasing signal|
| ModelLogLikelihood.m | binomial calculation of the quality of fit of the signal integration model to situs data following sham and actual suction intervention|
| ModelSignal.m | implementation of model of signal change due to intervention at 3, 4, 5, 6, 7, 8ss, 5+7ss and sham|
| RecoveryModel.m | model of recovery of anterior angular velocity following suction intervention|
| ShamModel.m | model of increase of anterior angular velocity following sham intervention|
| PrettyPrint.m | script to specify figure dimensions and format of axis label for printing|

### Output (supplementary information for inspection)

| Figure file | Description |
| :---        | :---        |
|AAV_data.png | data on anterior angular velocity with and without intervention at 5ss at times 6, 7 and 8ss |
|intervention_AAV_model.png | data and fitted regression model of anterior angular velocity following intervention|
| sham_AAV_model.png | data and fitted regression model of anterior angular velocity following sham intervention|
| sham_AAV_dist.png | simulated anterior angular velocity |distributions over time following sham intervention|
|interv3ss_AAV_dist.png | simulated anterior angular velocity distributions over time following intervention at 3 ss|
|interv4ss_AAV_dist.png, interv5ss_AAV_dist.png, interv6ss_AAV_dist.png, interv7ss_AAV_dist.png, interv8ss_AAV_dist.png, intervDble_AAV_dist.png | corresponding figures simulating intervention at 4, 5, 6, 7, 8 and 5+7ss|
|sham_AAV_dist.png | simulated anterior angular velocity distributions over time following sham intervention|
|sham_AAV_dist.png | simulated anterior angular velocity distributions over time following sham intervention|
|sham_AAV_dist.png | simulated anterior angular velocity distributions over time following sham intervention|
| sham_AAV_dist.png | simulated anterior angular velocity distributions over time following sham intervention|

### Output for Figure 6 of main manuscript:
| Figure file | Description |
| :---        | :---        |
| model_situs_<model_version>.png | best fit signal integration model output, for comparison to situs data|
| NormStageWeightings_<model_version>.png | associated stage weightings (sensitivity to flow)|
| NormStageContributions_<model_version>.png | associated stage contributions (importance in establishing situs)|
         
## Contributing
No further changes are expected to be made to this repository as the manuscript is now complete.

## Authors and acknowledgment
Code written by David J. Smith, incorporating data and intellectual input from all authors listed above.

## License
This work is licensed under a Creative Commons Attribution 4.0 International License: https://creativecommons.org/licenses/by/4.0/


