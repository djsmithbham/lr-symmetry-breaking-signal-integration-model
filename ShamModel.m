function omega=ShamModel(phi,t)

omega=phi(1)+phi(2)*t;