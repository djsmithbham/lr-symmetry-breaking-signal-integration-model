% main Matlab script to fit signal integration and intervention model
% 
% to accompany the article:
% Fluid extraction from the left-right organizer uncovers mechanical properties needed for symmetry breaking
%
% by 
%
% Pedro Sampaio, Sara Pestana, Catarina Bota, Adán Guerrero, Ivo A. Telley, David. J. Smith, Susana S. Lopes 
%
% currently under review for publication.
%
% Scripts were written for Matlab R2021a, Mathworks, Inc, Natick MA.
%
% Required Matlab toolboxes: Statistics and Machine Learning
%                            Optimization
%                            Global Optimization
%
% Overview
%
% This script reads in angular velocity data from intervention experiments along with
% data on motile cilia percentages over time
% then fits a signal integration model and the effect of intervention to
% the data
%
% The output consists of figure panels showing the best fit to data and
% associated parameter values (stage weightings), along with relative stage
% weightings multiplied by angular velocities (stage contributions)
% 
% The panels comprise Figure 6 of the main manuscript.
%
%
% Detailed summary
% 
% input data files: av_data.csv                   - angular velocity data
%                   motile_percentages_saved.csv  - data on changes in the percentate of cilia that are motile over time
%
%                   (note that data on number of embryos with defects and total number of embryos is given below within the code)
%
% variable to be specified by user: model_version 
%                                   can be set as 'full', 'no3ss', 'no3-4ss', 'no3-5ss', 'no3-6ss' to test the alternative models with no
%                                   contribution before 4, 5, 6 and 7ss for comparison
%                                   all of these alternatives were tested to generate Figure 6 of the main manuscript
%
% associated function files: ModelFun.m           - statistical model of defect rate under a given biasing signal
%                            ModelLogLikelihood.m - binomial calculation of the quality of fit of the signal integration model to situs data following sham and actual suction intervention
%                            ModelSignal.m        - implementation of model of signal change due to intervention at 3, 4, 5, 6, 7, 8ss, 5+7ss and sham
%                            RecoveryModel.m      - model of recovery of anterior angular velocity following suction intervention
%                            ShamModel.m          - model of increase of anterior angular velocity following sham intervention
%                            PrettyPrint.m        - script to specify figure dimensions and format of axis label for printing
%
% output (supplementary information for inspection):
%
%         AAV_data.png               - data on anterior angular velocity with and without intervention at 5ss at times 6, 7 and 8ss
%         intervention_AAV_model.png - data and fitted regression model of anterior angular velocity following intervention
%         sham_AAV_model.png         - data and fitted regression model of anterior angular velocity following sham intervention
%         sham_AAV_dist.png          - simulated anterior angular velocity distributions over time following sham intervention
%         interv3ss_AAV_dist.png     - simulated anterior angular velocity distributions over time following intervention at 3ss
%         interv4ss_AAV_dist.png, interv5ss_AAV_dist.png,
%         interv6ss_AAV_dist.png, interv7ss_AAV_dist.png,
%         interv8ss_AAV_dist.png, intervDble_AAV_dist.png
%                                    - corresponding figures simulating intervention at 4, 5, 6, 7, 8 and 5+7ss.
%         sham_AAV_dist.png          - simulated anterior angular velocity distributions over time following sham intervention
%         sham_AAV_dist.png          - simulated anterior angular velocity distributions over time following sham intervention
%         sham_AAV_dist.png          - simulated anterior angular velocity distributions over time following sham intervention
%         sham_AAV_dist.png          - simulated anterior angular velocity distributions over time following sham intervention
%
% output for Figure 6 of main manuscript:
%         model_situs_<model_version>.png            - best fit signal integration model output, for comparison to situs data
%         NormStageWeightings_<model_version>.png    - associated stage weightings (sensitivity to flow)
%         NormStageContributions_<model_version>.png - associated stage contributions (importance in establishing situs)
%         


clear all;

% plotting parameters
fs=9;
fn='times';
wd=8;
ht=6;

% number of samples
nsamp=1000;

% enables full model or 3ss/3-4ss/3-5ss/3-6ss to be set to zero
model_version='full';

% data from intervention experiments
defectno=[2 7 14 4 3 2 6];
Nemb=[13 26 40 27 25 20 16];

% baseline abnormality rate in the absence of intervention
baselineAbnormal=0.1;

X=csvread('av_data.csv');
Y=X;
Y(:,1)=Y(:,1)-5;
S=Y(find(Y(:,3)==0),:); %sham data
Z=Y(find(Y(:,3)==1),:); %intervention data

motile_percentages=csvread('motile_percentages_saved.csv'); % motile cilia percentages from eLife
rescale3ss=motile_percentages(1,2)/motile_percentages(4,2); % rescaling based on motility at 6ss
rescale4ss=motile_percentages(2,2)/motile_percentages(4,2);
rescale5ss=motile_percentages(3,2)/motile_percentages(4,2);

indiv_pred=[];
[FEParams,REVar,stats,RndEff]=nlmefit(Z(:,1),Z(:,4),Z(:,2),indiv_pred,...
                              @RecoveryModel,[2,1,0.5],'REParamsSelect',...
                              logical([0 0 1]),'optimfun','fminunc');
RESd=[0;0;sqrt(REVar)];
t=linspace(0,5.5,100);
stats.sebeta

figure(1);clf;hold on;
plot(Z(:,1)-0.04+5,Z(:,4),'kx','markersize',3);
plot(S(:,1)+0.04+5,S(:,4),'bo','markersize',3);
hl=legend('intervention','sham');set(hl,'fontsize',fs,'box','off','location','northoutside');
PrettyPrint(1,wd,ht,fs,'somite stage','anterior angular velocity','-dpng','AAV_data.png');

format long
FEParams
RESd

close(1);

[FEParamSham,REVarSham,statsSham]=nlmefit(S(:,1),S(:,4),S(:,2),indiv_pred,...
                              @ShamModel,[2 1],'REParamsSelect',...
                              logical([1 0]),'optimfun','fminunc');
RESdSham=[sqrt(REVarSham);0]
statsSham.sebeta

figure(1);clf;hold on;
plot(Z(:,1),Z(:,4),'ko','markersize',3);
plot(t,RecoveryModel(FEParams',t),'k','linewidth',2);
plot(t,RecoveryModel(FEParams'-RESd',t),'k','linewidth',1);
plot(t,RecoveryModel(FEParams'+RESd',t),'k','linewidth',1);
plot(t,RecoveryModel(FEParams'-2*RESd',t),'k--');
plot(t,RecoveryModel(FEParams'+2*RESd',t),'k--');
%title('intervention');
xlim([0 3.5]);
set(gca,'xtick',[0 1 2 3]);
set(gca,'xticklabel',{'5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','intervention_AAV_model.png');
close(1);

%%

figure(1);clf;hold on;
t=linspace(0.5,3.5,2);
plot(S(:,1)+5,S(:,4),'ko','markersize',3);
plot(t+5,ShamModel(FEParamSham',t),'k','linewidth',2);
plot(t+5,ShamModel(FEParamSham'-RESdSham',t),'k');
plot(t+5,ShamModel(FEParamSham'+RESdSham',t),'k');
plot(t+5,ShamModel(FEParamSham'-2*RESdSham',t),'k--');
plot(t+5,ShamModel(FEParamSham'+2*RESdSham',t),'k--');
xlim([5 8.5]);
set(gca,'xtick',[5 6 7 8]);
set(gca,'xticklabel',{'5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','sham_AAV_model.png');
close(1);

%%
% calculate distributions of anterior angular velocity
%
% sigSham3ss...sigSham8ss are the signals at absolute times 3ss...8ss with no
%   intervention, with rescaling applied basedon cilia density for 3ss, 4ss, 5ss
% sigRec is signal at times relative to intervention based on 5ss intervention data, which
%   is later (ModelSignal.m) rescaled based on cilia density at earlier
%   times

for j=1:nsamp
    sigRec(j,1)=RecoveryModel(FEParams'+RESd'*randn(1),1)';
    sigRec(j,2)=RecoveryModel(FEParams'+RESd'*randn(1),2)';
    sigRec(j,3)=RecoveryModel(FEParams'+RESd'*randn(1),3)';
    sigRec(j,4)=RecoveryModel(FEParams'+RESd'*randn(1),4)';
    sigRec(j,5)=RecoveryModel(FEParams'+RESd'*randn(1),5)';
    sigSham3ss(j,1)=rescale3ss*ShamModel(FEParamSham'+RESdSham'*randn(1),1)';
    sigSham4ss(j,1)=rescale4ss*ShamModel(FEParamSham'+RESdSham'*randn(1),1)';
    sigSham5ss(j,1)=rescale5ss*ShamModel(FEParamSham'+RESdSham'*randn(1),1)';
    sigSham6ss(j,1)=ShamModel(FEParamSham'+RESdSham'*randn(1),1)';
    sigSham7ss(j,1)=ShamModel(FEParamSham'+RESdSham'*randn(1),2)';
    sigSham8ss(j,1)=ShamModel(FEParamSham'+RESdSham'*randn(1),3)';
end

%%

% plot AAV distributions for each case

wd=8;
ht=5;
figure(1);clf;
shamData=[sigSham3ss;sigSham4ss;sigSham5ss;sigSham6ss;sigSham7ss;sigSham8ss];
grps=[3*ones(nsamp,1);4*ones(nsamp,1);5*ones(nsamp,1);6*ones(nsamp,1);7*ones(nsamp,1);8*ones(nsamp,1)];
boxplot(shamData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','sham_AAV_dist.png');
close(1);

zz=0*sigSham3ss;
wd=8;
ht=5;
figure(1);clf;
interv3ssData=[zz;rescale4ss*sigRec(:,1);rescale5ss*sigRec(:,2);sigRec(:,3);sigRec(:,4);sigRec(:,5)];
boxplot(interv3ssData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','interv3ss_AAV_dist.png');
close(1);

figure(1);clf;
interv4ssData=[sigSham3ss;zz;rescale5ss*sigRec(:,1);sigRec(:,2);sigRec(:,3);sigRec(:,4)];
boxplot(interv4ssData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','interv4ss_AAV_dist.png');
close(1);

figure(1);clf;
interv5ssData=[sigSham3ss;sigSham4ss;zz;sigRec(:,1);sigRec(:,2);sigRec(:,3)];
boxplot(interv5ssData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','interv5ss_AAV_dist.png');
close(1);

figure(1);clf;
interv6ssData=[sigSham3ss;sigSham4ss;sigSham5ss;zz;sigRec(:,1);sigRec(:,2)];
boxplot(interv6ssData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','interv6ss_AAV_dist.png');
close(1);

figure(1);clf;
interv7ssData=[sigSham3ss;sigSham4ss;sigSham5ss;sigSham6ss;zz;sigRec(:,1)];
boxplot(interv7ssData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','interv7ss_AAV_dist.png');
close(1);

figure(1);clf;
interv8ssData=[sigSham3ss;sigSham4ss;sigSham5ss;sigSham6ss;sigSham7ss;zz];
boxplot(interv8ssData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','interv8ss_AAV_dist.png');
close(1);
%%
figure(1);clf;
intervDbleData=[sigSham3ss;sigSham4ss;zz;sigRec(:,1);zz;sigRec(:,1)];
boxplot(intervDbleData,grps);
ylim([0 1.2]);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
PrettyPrint(1,wd,ht,fs,'time','anterior angular velocity (a.u.)','-dpng','intervDble_AAV_dist.png');
close(1);

%%
% likelihood version for each series
% interventions at: 3ss, 4ss, 5ss, 6ss, 7ss, 8ss, 5ss+7ss

% parameters of the model are:
%   weightings at 3ss, 4ss, 5ss, 6ss, 7ss, 8ss

% switch to enable 3ss/3-4ss/3-5ss/3-6ss weights to be set to zero

switch model_version
    case 'full'
        logLikelihoodLossFn=@(params)  -ModelLogLikelihood(params,sigRec,...
                                                  rescale3ss,rescale4ss,rescale5ss,...
                                                  sigSham3ss,sigSham4ss,sigSham5ss,...
                                                  sigSham6ss,sigSham7ss,sigSham8ss,...
                                                  nsamp,baselineAbnormal,defectno,Nemb);

        % initial guess equally-weighted
        params0=ones(1,6);
        problem=createOptimProblem('fmincon','x0',params0,'objective',logLikelihoodLossFn,'lb',zeros(1,6),'ub',5*ones(1,6));
        paramsMSOpt=run(MultiStart,problem,100000)
        logLikelihoodLossFn(paramsMSOpt)
        problem=createOptimProblem('fmincon','x0',paramsMSOpt,'objective',logLikelihoodLossFn,'lb',zeros(1,6),'ub',5*ones(1,6));
        paramsGSOpt=run(GlobalSearch,problem)
        logLikelihoodLossFn(paramsGSOpt)

        if logLikelihoodLossFn(paramsMSOpt)<logLikelihoodLossFn(paramsGSOpt)
            paramsGOpt=paramsMSOpt;
        else
            paramsGOpt=paramsGSOpt;
        end

    case 'no3ss'
                logLikelihoodLossFn=@(params)    -ModelLogLikelihood([0 params],sigRec,...
                                                  rescale3ss,rescale4ss,rescale5ss,...
                                                  sigSham3ss,sigSham4ss,sigSham5ss,...
                                                  sigSham6ss,sigSham7ss,sigSham8ss,...
                                                  nsamp,baselineAbnormal,defectno,Nemb);

        % initial guess equally-weighted
        params0=ones(1,5);
        problem=createOptimProblem('fmincon','x0',params0,'objective',logLikelihoodLossFn,'lb',zeros(1,5),'ub',5*ones(1,5));
        paramsMSOpt=run(MultiStart,problem,100000)
        logLikelihoodLossFn([0 paramsMSOpt])
        problem=createOptimProblem('fmincon','x0',paramsMSOpt,'objective',logLikelihoodLossFn,'lb',zeros(1,5),'ub',5*ones(1,5));
        paramsGSOpt=run(GlobalSearch,problem)
        logLikelihoodLossFn([0 paramsGSOpt])

        if logLikelihoodLossFn([0 paramsMSOpt])<logLikelihoodLossFn([0 paramsGSOpt])
            paramsGOpt=[0 paramsMSOpt];
        else
            paramsGOpt=[0 paramsGSOpt];
        end
        
    case 'no3-4ss'
                logLikelihoodLossFn=@(params)  -ModelLogLikelihood([0 0 params],sigRec,...
                                                  rescale3ss,rescale4ss,rescale5ss,...
                                                  sigSham3ss,sigSham4ss,sigSham5ss,...
                                                  sigSham6ss,sigSham7ss,sigSham8ss,...
                                                  nsamp,baselineAbnormal,defectno,Nemb);

        % initial guess equally-weighted
        params0=ones(1,4);
        problem=createOptimProblem('fmincon','x0',params0,'objective',logLikelihoodLossFn,'lb',zeros(1,4),'ub',5*ones(1,4));
        paramsMSOpt=run(MultiStart,problem,100000)
        logLikelihoodLossFn([0 0 paramsMSOpt])
        problem=createOptimProblem('fmincon','x0',paramsMSOpt,'objective',logLikelihoodLossFn,'lb',zeros(1,4),'ub',5*ones(1,4));
        paramsGSOpt=run(GlobalSearch,problem)
        logLikelihoodLossFn([0 0 paramsGSOpt])

        if logLikelihoodLossFn([0 0 paramsMSOpt])<logLikelihoodLossFn([0 0 paramsGSOpt])
            paramsGOpt=[0 0 paramsMSOpt];
        else
            paramsGOpt=[0 0 paramsGSOpt];
        end
        
    case 'no3-5ss'
                logLikelihoodLossFn=@(params)  -ModelLogLikelihood([0 0 0 params],sigRec,...
                                                  rescale3ss,rescale4ss,rescale5ss,...
                                                  sigSham3ss,sigSham4ss,sigSham5ss,...
                                                  sigSham6ss,sigSham7ss,sigSham8ss,...
                                                  nsamp,baselineAbnormal,defectno,Nemb);

        % initial guess equally-weighted
        params0=ones(1,3);
        problem=createOptimProblem('fmincon','x0',params0,'objective',logLikelihoodLossFn,'lb',zeros(1,3),'ub',5*ones(1,3));
        paramsMSOpt=run(MultiStart,problem,100000)
        logLikelihoodLossFn([0 0 0 paramsMSOpt])
        problem=createOptimProblem('fmincon','x0',paramsMSOpt,'objective',logLikelihoodLossFn,'lb',zeros(1,3),'ub',5*ones(1,3));
        paramsGSOpt=run(GlobalSearch,problem)
        logLikelihoodLossFn([0 0 0 paramsGSOpt])

        if logLikelihoodLossFn([0 0 0 paramsMSOpt])<logLikelihoodLossFn([0 0 0 paramsGSOpt])
            paramsGOpt=[0 0 0 paramsMSOpt];
        else
            paramsGOpt=[0 0 0 paramsGSOpt];
        end
        
    case 'no3-6ss'
                logLikelihoodLossFn=@(params)  -ModelLogLikelihood([0 0 0 0 params],sigRec,...
                                                  rescale3ss,rescale4ss,rescale5ss,...
                                                  sigSham3ss,sigSham4ss,sigSham5ss,...
                                                  sigSham6ss,sigSham7ss,sigSham8ss,...
                                                  nsamp,baselineAbnormal,defectno,Nemb);

        % initial guess equally-weighted
        params0=ones(1,2);
        problem=createOptimProblem('fmincon','x0',params0,'objective',logLikelihoodLossFn,'lb',zeros(1,2),'ub',5*ones(1,2));
        paramsMSOpt=run(MultiStart,problem,100000)
        logLikelihoodLossFn(paramsMSOpt)
        problem=createOptimProblem('fmincon','x0',paramsMSOpt,'objective',logLikelihoodLossFn,'lb',zeros(1,2),'ub',5*ones(1,2));
        paramsGSOpt=run(GlobalSearch,problem)
        logLikelihoodLossFn([0 0 0 0 paramsGSOpt])

        if logLikelihoodLossFn([0 0 0 0 paramsMSOpt])<logLikelihoodLossFn([0 0 0 0 paramsGSOpt])
            paramsGOpt=[0 0 0 0 paramsMSOpt];
        else
            paramsGOpt=[0 0 0 0 paramsGSOpt];
        end
end
%%
wd=6;
ht=4.5;

figure(1);clf;
hb=bar(paramsGOpt(1:end)/sum(paramsGOpt(1:end)));
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
title('normalised stage weightings');
PrettyPrint(1,wd,ht,fs,'stage','weighting','-dpng',['NormStageWeightings_' model_version '.png']);
close(1);

figure(1);clf;
Contribs=mean(paramsGOpt(1:end).*[sigSham3ss sigSham4ss sigSham5ss sigSham6ss sigSham7ss sigSham8ss]);
normContribs=Contribs/sum(Contribs);
hb=bar(normContribs);
set(gca,'xticklabel',{'3ss','4ss','5ss','6ss','7ss','8ss'});
set(gca,'fontname',fn);
title('normalised signal contribution');
PrettyPrint(1,wd,ht,fs,'stage','contribution','-dpng',['NormStageContributions_' model_version '.png']);
close(1);

figure(1);clf;
abfreqs=ModelFun(paramsGOpt,sigRec,rescale3ss,rescale4ss,rescale5ss,...
                 sigSham3ss,sigSham4ss,sigSham5ss,sigSham6ss,sigSham7ss,sigSham8ss,...
                 nsamp,baselineAbnormal);
normfreqs=1-abfreqs;
hb=bar([normfreqs(1:end-1)' abfreqs(1:end-1)'],'stacked');
set(gca,'xticklabel',{'3ss', '4ss', '5ss', '6ss', '7ss', '8ss', '5+7ss'}); % ,'sham' - don't plot sham for now
xlim([0.5,7.5]);
set(gca,'fontname',fn);
hl=legend('normal','defect');
set(hl,'location','south');
PrettyPrint(1,wd,ht,fs,'intervention','frequency','-dpng',['model_situs_' model_version '.png']);

close(1);