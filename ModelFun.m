function probAbnormal=ModelFun(params,sigRec,rescale3ss,rescale4ss,rescale5ss,...
                             sigSham3ss,sigSham4ss,sigSham5ss,sigSham6ss,sigSham7ss,... 
                             sigSham8ss,nsamp,baselineAbnormal)

% model for probability of observed number of defects with given signal                         
                         
% params = weightings of each stage, and signal threshold for weighted sum
% aa, aa3, aa4, aa5, aa6, aa7 - angular velocity model at each stage
% (aa=with intervention, aaN=without intevention)

[sig_interv3ss,sig_interv4ss,sig_interv5ss,sig_interv6ss,...
          sig_interv7ss,sig_interv8ss,sig_double,sig_sham]= ...
          ModelSignal(params,sigRec,rescale3ss,rescale4ss,rescale5ss,...
                      sigSham3ss,sigSham4ss,sigSham5ss,sigSham6ss,sigSham7ss,sigSham8ss);
p3ssAb=sum(sig_interv3ss<1)/nsamp;
p4ssAb=sum(sig_interv4ss<1)/nsamp;
p5ssAb=sum(sig_interv5ss<1)/nsamp;
p6ssAb=sum(sig_interv6ss<1)/nsamp;
p7ssAb=sum(sig_interv7ss<1)/nsamp;
p8ssAb=sum(sig_interv8ss<1)/nsamp;
pDblAb=sum(sig_double<1)/nsamp;
pShmAb=sum(sig_sham<1)/nsamp;
probAbnormal=    baselineAbnormal...
               +(0.5-baselineAbnormal)*[p3ssAb p4ssAb p5ssAb p6ssAb p7ssAb p8ssAb pDblAb pShmAb];
               % the 0.5 is because at worst half will be abnormal
